﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.IO;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System.Net.Http;

namespace DiVa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecognitionController : ControllerBase
    {
        // POST api/recognition
        [HttpPost]
        public IActionResult Post([FromQuery] string culture = "de-de")
        {
            SpeechRecognitionEngine speechRecognition = SelectRecognizer(CultureInfo.GetCultureInfo(culture));
            speechRecognition.LoadGrammar(new DictationGrammar());

            long? length = Request.ContentLength;
            byte[] data = new byte[(int)length];
            int read = 0;
            while (read < length)
                read += Request.Body.Read(data, read, Math.Min(10240, (int)length - read));

            if (Request.ContentType == "audio/wav")
            {
                speechRecognition.SetInputToWaveStream(new MemoryStream(data));
            }
            else
            {
                Ok(new { Success = false, Message = "You need to supply Audio data as audio/wav" });
            }
           
            RecognitionResult result = speechRecognition.Recognize();

            data = null;
            speechRecognition = null;

            return GetTTS(result.Text);
        }

        //nhr6SqhPFFs956

        // GET api/recognition/cultures
        [HttpGet]
        public IActionResult GetCultures()
        {
            return Ok(SpeechRecognitionEngine.InstalledRecognizers().Select(c => c.Culture).ToArray());

        }

        private SpeechRecognitionEngine SelectRecognizer(CultureInfo requiredCulture)
        {
            // Select based on a specific recognizer configuration  
            SpeechRecognitionEngine speechRecognitionEngine = null;
            foreach (RecognizerInfo config in SpeechRecognitionEngine.InstalledRecognizers())
            {
                if (config.Culture.Equals(requiredCulture))
                {
                    speechRecognitionEngine = new SpeechRecognitionEngine(config);
                    break;
                }
            }
            return speechRecognitionEngine;
        }

        public FileResult GetTTS(string tts)
        {
            SpeechSynthesizer reader = new SpeechSynthesizer();
            MemoryStream stream = new MemoryStream();
            reader.SetOutputToWaveStream(stream);
            reader.Speak(tts);

            stream.Position = 0;

            FileStreamResult result = new FileStreamResult(stream,"audio/wav");
            return result;
        }
    }
}
